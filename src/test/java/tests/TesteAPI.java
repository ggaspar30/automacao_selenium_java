package tests;
import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
import io.restassured.http.ContentType;
import org.junit.BeforeClass;
import org.junit.Test;

public class TesteAPI {
    @BeforeClass
    public static void setup() {

     baseURI = "http://localhost";
     port = 8080;
     basePath = "/api";

    }

    @Test
    public void deveConsultaCpfComRestricoes() {
            given()
                .when()
                .get("/v1/restricoes/60094146012")
                .then()
                    .log().all()
                    .assertThat()
                        .statusCode(200);
    }
    @Test
    public void deveCriarUmaSimulacao() {
        given()
                .body("{\n" +
                        "  \"nome\": \"TESTE QA\",\n" +
                        "  \"cpf\": 970932300333,\n" +
                        "  \"email\": \"email@email.com\",\n" +
                        "  \"valor\": 1500,\n" +
                        "  \"parcelas\": 5,\n" +
                        "  \"seguro\": true\n" +
                        "}")
                .contentType(ContentType.JSON)
        .when()
                .post("/v1/simulacoes")
        .then()
                .log().all()
                .assertThat()
                            .statusCode(201);
    }
    @Test
    public void deveAlterarUmaSimulacao() {
        given()
                .body("{\n" +
                        "  \"nome\": \"TESTE de Alterar\",\n" +
                        "  \"cpf\": 97093230000,\n" +
                        "  \"email\": \"email@email.com\",\n" +
                        "  \"valor\": 1500,\n" +
                        "  \"parcelas\": 5,\n" +
                        "  \"seguro\": true\n" +
                        "}")
                .contentType(ContentType.JSON)
        .when()
                .put("/v1/simulacoes/97093230000")
        .then()
                .log().all()
                .assertThat()
                .statusCode(200);
    }
    @Test
    public void deveConsultarTodasSimulacoes() {
     given()
         .when()
                .get("/v1/simulacoes")
        .then()
                .log().all()
                 .assertThat()
                    .statusCode(200);
    }
    @Test
    public void deveConsultarUmaSimulacao() {
        given()
                .when()
                .get("/v1/simulacoes/97093236014")
                .then()
                .log().all()
                .assertThat()
                .statusCode(200);
    }
    @Test
    public void deveRemoverUmaSimulacao() {
        given()
                .when()
                    .delete("/v1/simulacoes/14")
                .then()
                    .log().all()
                        .assertThat()
                            .statusCode(200);
    }

}