package tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import br.teste.sicred.qa.core.BaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HomeTest extends BaseTest {

    @Before
    public void deveAbriNavegador(){
    inicializa();
}
    @Test
    public void testDeveAcessarCadatroDeUsuarios(){
        trocandoTema();
        inserindoDadosUsuario();
        esperar(3000);
        Assert.assertEquals(SUCCESS, getMessageSuccess());
        voltaParaHome();
        pesquisaCustomerName();
       esperar(3000);
        apagaCustumer();
        Assert.assertEquals(ALERT, getMessageAlertDeleted());
       // Assert.assertEquals(SUCCESSFULLY_DELETED, getMessageDeletedSuccess());
    }
    @After
    public  void  deveFecharNavegador(){
        //fechar o navegador
        finaliza();
    }

}

