package br.teste.sicred.qa.core;



import io.opentelemetry.exporter.logging.SystemOutLogRecordExporter;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTest {

    public static final String URL_BASE = "https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap";
        public static String NAME = "Teste Sicredi";
    public static String LASTNAME ="Teste";
    public static String  CONTACTFIRSTNAME = "Gustavo Gaspar";
    public static final String PHONE = "51 99999-9999";
    public static final String ADDRESSLINE1 = "Av Assis Brasil, 3970";
    public static final String ADDRESSLINE2 = "Torre D";
    public static final String CITY = "Porto Alegre";
    public static final String STATE = "RS";
    public static final String POSTALCODE = "91000-000";
    public static final String COUNTRY = "Brasil";
    public static final String SALESREPEMPLOYEENUMBER = "001555";
    public static final String CREDITLIMIT = "200";
    public static final String SUCCESS = "Your data has been successfully stored into the database. Edit Record or Go back to list";
    public static final String SUCCESSFULLY_DELETED = "Your data has been successfully deleted from the databas";

    public static final String ALERT = "Are you sure that you want to delete this 1 item?";

    private WebDriver navegador;




    public void inicializa(){

        WebDriverManager.chromedriver().setup();

        navegador = new ChromeDriver();
        navegador.manage().window().maximize();
        navegador.get(URL_BASE);
//        System.setProperty("Webdriver.chrome.driver", "src\\main\\resources\\driver\\chromedriver.exe");
//        navegador = new ChromeDriver(); // abrindo o navegador


        //Navegando para a pagina


    }


    public void finaliza() {
        navegador.quit();
        }
   public void esperar(long tempo) {
       try {
           Thread.sleep(tempo);
       } catch (InterruptedException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
       }

   }
    public void trocandoTema(){
        WebElement select = navegador.findElement(By.id("switch-version-select"));
        new Select(select).selectByVisibleText("Bootstrap V4 Theme");

        navegador.findElement(By.xpath("//*[@id=\"switch-version-select\"]/option[4]")).click();
        navegador.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[1]/div[1]/a")).click();

    }

    public void inserindoDadosUsuario(){
        navegador.findElement(By.id("field-customerName")).sendKeys(NAME);
        navegador.findElement(By.name("contactLastName")).sendKeys(LASTNAME);
        navegador.findElement(By.name("contactFirstName")).sendKeys(CONTACTFIRSTNAME);
        navegador.findElement(By.name("phone")).sendKeys(PHONE);
        navegador.findElement(By.name("addressLine1")).sendKeys(ADDRESSLINE1);
        navegador.findElement(By.name("addressLine2")).sendKeys(ADDRESSLINE2);
        navegador.findElement(By.name("city")).sendKeys(CITY);
        navegador.findElement(By.name("state")).sendKeys(STATE);
        navegador.findElement(By.name("postalCode")).sendKeys(POSTALCODE);
        navegador.findElement(By.name("country")).sendKeys(COUNTRY);
        navegador.findElement(By.name("salesRepEmployeeNumber")).sendKeys(SALESREPEMPLOYEENUMBER);
        navegador.findElement(By.name("creditLimit")).sendKeys(CREDITLIMIT);
        navegador.findElement(By.id("form-button-save")).click();



    }
    public void voltaParaHome(){
        navegador.findElement(By.xpath("//*[@id=\"report-success\"]/p/a[2]")).click();

    }
    public void pesquisaCustomerName(){
        navegador.findElement(By.name("customerName")).sendKeys("Teste Sicredi");
        navegador.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[2]/table/thead/tr[2]/td[2]/div[2]/a"));

    }
    public void apagaCustumer(){
        navegador.findElement(By.xpath("//input[@type='checkbox']")).click();
        navegador.findElement(By.linkText("Delete")).click();
        esperar(2000);
        navegador.findElement(By.xpath("/html/body/div[2]/div[2]/div[3]/div/div/div[3]/button[2]")).click();

    }
    public String getMessageSuccess() {
        String MessageSuccess = navegador.findElement(By.id("report-success")).getText();
        return MessageSuccess;
    }
    public String getMessageAlertDeleted() {
        WebElement modal = navegador.findElement(By.className("delete-multiple-confirmation"));
        String MessageAlertDeleted = modal.findElement(By.className("modal-body")).getText();

        return MessageAlertDeleted ;

    }
    //não consegui validar esse assert
    public String getMessageDeletedSuccess() {

        WebElement alert = navegador.findElement(By.xpath("xpath=(.//*[normalize-space(text()) and normalize-space(.)='Stop'])[1]/following::span[1]"));
        String alertSuccess = alert.getText();
        return alertSuccess;
    }


}

