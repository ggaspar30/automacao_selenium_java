# Automação de Teste

Projeto de automação com selenium webdriver java e Restassured  


## ⚙️ Executando os testes

Executar a classe Home pra a execução de testes


## 📦 Implantação

Adicione notas adicionais sobre como implantar isso em um sistema ativo

## 🛠️ Construído com

Java jdk 19

* [Selenium](https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-java) - Usado para automação web
* [Maven](https://maven.apache.org/) - Gerente de Dependência
* [Rest-assured](https://mvnrepository.com/artifact/io.rest-assured/rest-assured/5.3.0) - Usado para gerar teste de API
* [JUnit](https://mvnrepository.com/artifact/junit/junit/4.13.2) - Usada para estruturar os testes


## ✒️ Autores

Gustavo Gaspar


- 😊
